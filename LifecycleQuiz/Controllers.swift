//
//  ViewController.swift
//  LifecycleQuiz
//
//  Created by Pavel Tsurbeleu on 5/13/15.
//  Copyright (c) 2015 Pavel Tsurbeleu. All rights reserved.
//

import UIKit

class A: LFViewController {
    // Just a placeholder, since the base class does all the logging
}

class B: LFViewController {
    // Just a placeholder, since the base class does all the logging
}

class LFViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logme()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        logme()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        logme()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        logme()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        logme()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        logme()
    }
    
    override func viewControllerForUnwindSegueAction(action: Selector, fromViewController: UIViewController, withSender sender: AnyObject?) -> UIViewController? {
        logme()
        return super.viewControllerForUnwindSegueAction(action, fromViewController: fromViewController, withSender: sender)
    }
    
    //
    func logme(functionName: String = __FUNCTION__) {
        let className = NSStringFromClass(self.classForCoder)
        println("in \(className).\(functionName)...")
    }
    
}